const express = require('express')
const session = require('express-session')
const app = express()
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// used for generating an oauth signature
var oauthSignature = require('oauth-signature')

// this is for oauth
var Grant = require('grant-express')
grant = new Grant({
  "server": {
    "protocol": "http",
    "host": "localhost:3001"
  },
  "intuit": {
    "key": "qyprdBumVM1VnDeomh8Qyh4LOu5nzi",
    "secret": "gmIvGbLaU24Oko3ElSmnpx7iscDjX2GSjEi98Mj0",
    "scope": ['read', 'write'],
     "callback": "/handle_intuit_callback"
  }})

// view engine
app.set('view engine', 'pug')

// this is for oauth
app.use(session( { secret: 'grant' } ) );
app.use(grant)

// oauth secret stuff
var OAUTH_ACCESS_SECRET = "nRcFMDmAFZiG0X36MlbgfqhRZZ92NlPU2BVVxVTa"
var OAUTH_ACCESS_TOKEN = "lvprdKTAW3OzPhqLzgYl4kQrdT3PSe8eaeGdh0ttZQCyFczC"
var OAUTH_AUTH_TOKEN_SECRET = "nRcFMDmAFZiG0X36MlbgfqhRZZ92NlPU2BVVxVTa"
var OAUTH_TOKEN = "lvprdKTAW3OzPhqLzgYl4kQrdT3PSe8eaeGdh0ttZQCyFczC"
var OAUTH_REALM_ID = "193514538239149"
var OAUTH_CONSUMER_KEY = "qyprdBumVM1VnDeomh8Qyh4LOu5nzi"
var OAUTH_CONSUMER_SECRET = "gmIvGbLaU24Oko3ElSmnpx7iscDjX2GSjEi98Mj0"

// use quickbooks npm to connect to intuit api
var QuickBooks = require('node-quickbooks')
var qbo = new QuickBooks(OAUTH_CONSUMER_KEY,
                         OAUTH_CONSUMER_SECRET,
                         OAUTH_TOKEN,
                         OAUTH_AUTH_TOKEN_SECRET,
                         OAUTH_REALM_ID,
                         true, // use the sandbox?
                         true); // enable debugging?

var nonceGen = require('nonce')();

// before quickbooks npm lol
var generateOauthHeader = function(url) {
  var timestamp = new Date() / 1000;
  timestamp = Math.floor(timestamp);
  var nonce = nonceGen();  

  var httpMethod = 'GET'
  var parameters = {
    oauth_consumer_key : OAUTH_CONSUMER_KEY,
    oauth_token : OAUTH_TOKEN,
    oauth_nonce : nonce,
    oauth_timestamp : timestamp,
    oauth_signature_method : 'HMAC-SHA1',
    oauth_version : '1.0',
  }
  
  var consumerSecret = OAUTH_CONSUMER_SECRET
  var tokenSecret = OAUTH_ACCESS_SECRET
  var encodedSignature = oauthSignature.generate(httpMethod, url, parameters, consumerSecret, tokenSecret)
  var signature = oauthSignature.generate(httpMethod, url, parameters, consumerSecret, tokenSecret, { encodeSignature: false})

  var header = "OAuth "
  header += "oauth_consumer_key=" + OAUTH_CONSUMER_KEY + ", "
  header += "oauth_token=" + OAUTH_TOKEN + ", "
  header += "oauth_signature=" + encodedSignature + ", "
  header += "oauth_nonce=" + nonce + ", "
  header += "oauth_timestamp=" + timestamp + ", "
  header += "oauth_signature_method=HMAC-SHA1, "
  header += "oauth_version=1.0"

  return header
} 

// mongo db
var MongoClient = require('mongodb').MongoClient
var mongoUrl = 'mongodb://localhost:27017/qbo'
MongoClient.connect(mongoUrl, function (err, db) {
  if (err) throw err
  db.createCollection("customers", function(err, res) {
    if (err) throw err
    console.log(res)
  })
  db.createCollection("bills", function(err, res) {
    if (err) throw err
    console.log(res)
  })
  db.createCollection("invoices", function(err, res) {
    if (err) throw err
    console.log(res)
  })
})

// save data to db
var saveCollectionToDatabase = function(data, collection) {
  MongoClient.connect(mongoUrl, function(err, db) {
    if (err) throw err;
    db.collection(collection).insertMany(data, function(err, res) {
      if (err) throw err;
      console.log("Number of records inserted: " + res.insertedCount);
      db.close();
    });
  });
}

var syncCustomers = function() {
  qbo.findCustomers({
    fetchAll: true
    }, function(e, customers) {
    
    // remove all from customers before syncing
    MongoClient.connect(mongoUrl, function(err, db) {
      if (err) throw err;
      var myquery = {};
      db.collection("customers").deleteMany(myquery, function(err, obj) {
        if (err) throw err;
        console.log(obj.result.n + " document(s) deleted");
        db.close();
      });
    });

    saveCollectionToDatabase(customers['QueryResponse']['Customer'], 'customers')
  })  
}

var syncBills = function() {
  qbo.findBills({
    fetchAll: true
    }, function(e, bills) {
    
    // remove all from bills before syncing
    MongoClient.connect(mongoUrl, function(err, db) {
      if (err) throw err;
      var myquery = {};
      db.collection("bills").deleteMany(myquery, function(err, obj) {
        if (err) throw err;
        console.log(obj.result.n + " document(s) deleted");
        db.close();
      });
    });

    saveCollectionToDatabase(bills['QueryResponse']['Bill'], 'bills')
  })  
}

var syncInvoices = function() {
  qbo.findInvoices({
    fetchAll: true
    }, function(e, invoices) {
    
    // remove all from bills before syncing
    MongoClient.connect(mongoUrl, function(err, db) {
      if (err) throw err;
      var myquery = {};
      db.collection("invoices").deleteMany(myquery, function(err, obj) {
        if (err) throw err;
        console.log(obj.result.n + " document(s) deleted");
        db.close();
      });
    });

    saveCollectionToDatabase(invoices['QueryResponse']['Invoice'], 'invoices')
  })  
}

// sync with qbo 
var syncWithQbo = function() {
  syncCustomers()
  syncBills()
  syncInvoices()
}

// express routes

// used for uauth
app.get('/handle_intuit_callback', function (req, res) {
  res.send("something happened maybe?")
})

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html')
})

app.get('/customers', function (req, res) {
  MongoClient.connect(mongoUrl, function(err, db) {
    if (err) throw err;
    db.collection("customers").find({}).toArray(function(err, result) {
      if (err) throw err;
      records = []
      console.log(result)
      for (var i = 0; i < result.length; i++) {
        records.push([ result[i]['GivenName'],  result[i]['FamilyName'], result[i]['ShipAddr'] ? result[i]['ShipAddr']['Line1'] : 'No Address', result[i]['PrimaryPhone'] ? result[i]['PrimaryPhone']['FreeFormNumber'] : 'No Phone Number', result[i]['_id'] ])
      }      
      var tableHeaders = ['FirstName', 'LastName', 'Address', 'PhoneNumber', 'MongoID']
      res.render('records', { title: 'Customers', tableHead: tableHeaders, recs: records })
      db.close();
    });
  });
})

// create a new customer
app.post('/customers', function (req, res) {
  newCustomerData = { Taxable: false,
    BillAddr: 
     { Line1: req.body.Address },
    Job: false,
    BillWithParent: false,
    Balance: 0,
    BalanceWithJobs: 0,
    CurrencyRef: { value: 'CAD', name: 'Canadian Dollar' },
    PreferredDeliveryMethod: 'None',
    domain: 'QBO',
    sparse: false,
    GivenName: req.body.FirstName,
    FamilyName: req.body.LastName,
    Active: true,
    PrimaryPhone: { FreeFormNumber: req.body.PhoneNumber } 
  }

  // create customer on quickbooks
  qbo.createCustomer(newCustomerData, function (e, customer) {
    console.log(customer)
  })

  // sync customers again
  syncCustomers();
  res.redirect('/customers')
})

app.get('/bills', function (req, res) {
  MongoClient.connect(mongoUrl, function(err, db) {
    if (err) throw err;
    db.collection("bills").find({}).toArray(function(err, result) {
      if (err) throw err;
      console.log(result)
      var records = []
      for (var i = 0; i < result.length; i++) {
        records.push([ result[i]['VendorRef']['name'], result[i]['TxnDate'], result[i]['DueDate'], result[i]['Balance'], result[i]['_id'] ])
      }      
      var tableHeaders = ['Payee', 'BillDate', 'DueDate', 'Amount', 'MongoID']
      res.render('records', { title: 'Bills', tableHead: tableHeaders, recs: records })
      db.close();
    });
  });  
})

app.post('/bills', function (req, res) {
  newBillData = { DueDate: req.body.DueDate,
    Balance: req.body.Amount,
    HomeBalance: req.body.Amount,
    domain: 'QBO',
    sparse: false,
    TxnDate: req.body.BillDate,
    Line: ['Best Product Evar'],
    CurrencyRef: { value: 'CAD', name: 'Canadian Dollar' },
    VendorRef: { value: '62', name: req.body.Payee },
    APAccountRef: { value: '31', name: 'Accounts Payable' },
    TotalAmt: req.body.Amount,
    GlobalTaxCalculation: 'TaxExcluded' 
  } 

  qbo.createBill(newBillData, function(e, bill) {
    console.log(bill)
  })
})

app.get('/invoices', function (req, res) {
  MongoClient.connect(mongoUrl, function(err, db) {
    if (err) throw err;
    db.collection("invoices").find({}).toArray(function(err, result) {
      if (err) throw err;
      console.log(result)
      var records = []
      for (var i = 0; i < result.length; i++) {
        records.push([ result[i]['CustomerRef']['name'], result[i]['TxnDate'], result[i]['DueDate'], result[i]['TotalAmt'], result[i]['Balance'], result[i]['_id'] ])
      }      
      var tableHeaders = ['Customer', 'OrderDate', 'DueDate', 'TotalAmount', 'BalanceAmount', 'MongoID']
      res.render('records', { title: 'Invoices', tableHead: tableHeaders, recs: records })
      db.close();
    });
  });  
})

app.get('/sync', function (req, res) {
  console.log('GET for Sync')
  syncWithQbo();
  res.redirect('/')
})

app.listen(3001, function () {
  console.log('Example app listening on port 3001!')
})